<!DOCTYPE html>
<html lang="en">

<head>

  <?php include('head.html') ?>

  <SCRIPT language="JavaScript">
    var currentGun;
    var deploying = false;

    function deployGun() {
      if(deploying){
        alert("You are already deploying a Gatling Gun. Please wait.");
        return;
      }

      var addr = web3.eth.accounts[0];
      if (confirm("You are about to deploy a Gatling Gun for the following address:\n" + addr + "\n\nThis address will become the owner of the Gatling Gun. Do you wish to continue?")){
        gunDeployer.deployGatlingGun(web3.eth.accounts[0], (err, res) => {
          if(err == undefined){
            deploying = true;
            $("#deploymentButton").text("Deploying, please wait ... This can take several minutes.");
          }
        });
      }
    }

    function detectNetwork(err, netId) {
      gunDeploymentAddress = addresses[netId]['gunDeploymentAddress'];
      gunDeployer = web3.eth.contract(gunDeploymentABI).at(gunDeploymentAddress);
      gunDeployer.NewGunDeployed().watch(deploymentListener);
      $("#selector").slideDown();
    }

    function deploymentListener(err, res){
      if (res.args['ownerAddress'] != web3.eth.accounts[0] || deploying == false){
        return;
      }
      $("#deploymentButton").text("Deploy New Gatling Gun");
      deploying = false;
      if(err){
        alert("Failed to deploy: " + err);
      }else{
        openGun(res.args['gunAddress']);
      }
    }

    function openGun(gunAddress){
      $("#selector").slideUp(); $("#opps").slideDown()
      currentGun = web3.eth.contract(gunABI).at(gunAddress);
      $("#gundisplayaddress").text(currentGun.address);
      currentGun.version((err, res) => {
        if (err === null){
          $("#gunversion").text(res);
        } else {
          $("#gunversion").text(' ?');
        }
      });
      currentGun.owner((err, res) => {
        if(res == web3.eth.accounts[0]){
          $("#areyouowner").html('<span class="badge badge-success">YES</span>');
        } else {
          $("#areyouowner").html('<span class="badge badge-danger">NO</span>');
        }
      });
      currentGun.authorized(web3.eth.accounts[0], (err, res) => {
        if(res){
          $("#areyouauthorized").html('<span class="badge badge-success">YES</span>');
        } else {
          $("#areyouauthorized").html('<span class="badge badge-danger">NO</span>');
        }
      });

    }

    function unauthorize(address) {
      currentGun.unauthorize(address, (err, res) => {
        if(err != undefined) {
          alert("FAILED to unauthorize " + address + "!");
        } else {
          alert("Successfuly removed " + address + " from the authorized wallets.  The blockchain is processing this now...");
        }
      })
    }

    function authorize(address) {
      currentGun.authorize(address, (err, res) => {
        if(err != undefined) {
          alert("FAILED to authorize " + address + "!");
        } else {
          alert("Successfuly added " + address + " to the authorized wallets. The blockchain is processing this now...");
        }
      })
    }

    function transferOwnership(address) {
      currentGun.setOwner(address, (err, res) => {
        if(err != undefined) {
          alert("FAILED to transfer ownership to " + address + "!");
        } else {
          alert("Successfuly transfered ownership to " + address + ".  The blockchain is processing this now...");
        }
      })
    }

    function copyToClipboard(elementId) {

      // Create a "hidden" input
      var aux = document.createElement("input");

      // Assign it the value of the specified element
      aux.setAttribute("value", document.getElementById(elementId).innerHTML);

      // Append it to the body
      document.body.appendChild(aux);

      // Highlight its content
      aux.select();

      // Copy the highlighted text
      document.execCommand("copy");

      // Remove it from the body
      document.body.removeChild(aux);

      $("#copynotify").show().delay(1000).fadeOut();

    }

    function addTransactions() {
      var raw = $("#bullets").val();
      var bullets = raw.split(";");
      for (bullet in bullets) {
        var data = bullets[bullet].trim().split('-');
        data = {
          target: data[0].trim(),
          value: data[1].trim(),
          payload: data[2].split('x')[1].trim()
        }
        
        var prettyPayload = data['payload'];
        if(prettyPayload.length > 20) {
          prettyPayload = prettyPayload.substring(0,20) + '...';
        }

        var row = $('<tr/>');
        
        row.html('<td>' + data['target'] + '</td><td>' + data['value'] + '</td><td>0x' + prettyPayload + '</td><td><a onclick="this.parentNode.parentNode.parentNode.removeChild(this.parentNode.parentNode);" href="#">[remove]</a></td>');
        
        $("#magazine").append(row);
        
        row.data('transaction', data);
      }
      var raw = $("#bullets").val("");
    }

    var gunDeployer;

    window.addEventListener('load', function () {
        if (typeof web3 !== 'undefined') {
            ethereum.enable();
            console.log('Web3 Detected! ' + web3.currentProvider.constructor.name);
            window.web3 = new Web3(web3.currentProvider);
            web3.version.getNetwork(detectNetwork);
        } else {
            alert("Metamask not installed! This site requires metamask!");
        }
    })

    function fire(){
      rows = $('TR');
      lengths = [];
      payloads = "";
      targets = [];
      values = [];
      for (var i = 0; i < rows.length; i++) {
        row = $(rows[i]);
        t = row.data('transaction');
        if(t === undefined){
          continue;
        }
        lengths.push(t['payload'].length / 2);
        payloads += t['payload'];
        targets.push(t['target']);
        values.push(t['value'])
      }
      currentGun.fire('0x' + payloads, targets, lengths, values, (err, res) => {
        if (err !== null){
          alert("Could not fire transactions!");
        } else {
          document.getElementById('magazine').innerHTML = '';
        }
      });
    }

  </SCRIPT>

</head>

<body>
  <?php include('menu.html') ?>
  <!-- Page Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-left">
        <h1 class="mt-3">Operation</h1>
      </div>
      <div class="col-lg-12 text-left" id="unknownNetwork" style="display: none">
        The Ethereum network you have set up in Metamask is not supported. Please use one of the following:
        <UL>
          <LI>Mainnet</LI>
          <LI>Ropsten</LI>
          <LI>Rinkeby</LI>
        </UL>
      </div>
      <div class="col-lg-12 text-left" id="selector" style="display: none">
        <p>To create a new Gatling Gun wallet for yourself, simply click the button below:</p>
        <p><button type="button" class="btn btn-danger" id="deploymentButton" onclick="deployGun()">Deploy New Gatling Gun</button></p>
        
        <p>If you already have a Gatling Gun wallet, you can connect to it by entering it's address below:</p>
          <form class="form">
            <div class="form-group mb-2">
              <label for="ggaddress" class="sr-only">Gatling Gun Address</label>
              <input type="text" class="form-control" id="ggaddress" placeholder="Gatling Gun Address" pattern="^0x[0-9a-fA-F]{40,40}$" title="Must be a valid ethereum address!">
            </div>
            <button type="button" class="btn btn-primary mb-2" onclick='openGun($("#ggaddress").val())'>Connect to Gatling Gun</button>
          </form>
      </div>
      <div class="col-lg-12 text-left" style="display: none" id="opps">
        <P>
          <button type="button" class="btn btn-light" onclick="window.location='operation.php';">&times;</button>
          <STRONG>Gun (v<span id="gunversion"></span>) at: </STRONG><button type="button" class="btn btn-light" id="gundisplayaddress" onclick="copyToClipboard('gundisplayaddress')"></button><span id='copynotify' style='display:none'>Copied to clipboard</span>
        </P>

        <h5>Are you the owner of this Gatling Gun: <SPAN id='areyouowner'></SPAN></h5>

        <P></P>
        <form class="form-inline">
          <div class="form-group mb-2">
            <label for="operatorAddress" class="sr-only">Operator Address</label>
            <input type="text" class="form-control" id="operatorAddress" placeholder="Operator Address" pattern="^0x[0-9a-fA-F]{40,40}$" title="Must be a valid ethereum address!">
          </div>&nbsp;
          <button type="button" class="btn btn-secondary mb-2" onclick='authorize($("#operatorAddress").val())'>Authorize</button>&nbsp;
          <button type="button" class="btn btn-secondary mb-2" onclick='unauthorize($("#operatorAddress").val())'>Unauthorize</button>
        </form>
        <P></P>
        <form class="form-inline">
          <div class="form-group mb-2">
            <label for="operatorAddress" class="sr-only">New Owner</label>
            <input type="text" class="form-control" id="newowneraddress" placeholder="New Owner Address" pattern="^0x[0-9a-fA-F]{40,40}$" title="Must be a valid ethereum address!">
          </div>&nbsp;
          <button type="button" class="btn btn-secondary mb-2" onclick='transferOwnership($("#newowneraddress").val())'>Transfer Ownership</button>
        </form>
        <P></P>
        <h5>Are you authorized to operate this Gatling Gun: <SPAN id='areyouauthorized'></SPAN></h5>
        <P></P>
          <div class="input-group">
              <button class="btn btn-info" type="button" onclick="addTransactions()">Add Transaction(s)</button>
             <input type="text" class="form-control" id="bullets" placeholder="Address - Value (wei) - Payload [ ; Address - Value (wei) - Payload]*">
          </div>

          <table class="table">
          <thead>
            <tr>
              <th scope="col">Target</th>
              <th scope="col">Value</th>
              <th scope="col">Payload</th>
              <th scope="col">Remove</th>
            </tr>
          </thead>
          <tbody id="magazine"></tbody>
        </table>
        <CENTER><button type="button" class="btn btn-danger w-50" onclick="fire()">FIRE!</button></CENTER>
      </div>
    </div>
  </div>
  <?php include('footer.html') ?>
</body>

</html>
