
const gunABI = [{"constant":false,"inputs":[{"name":"wallet","type":"address"}],"name":"setOwner","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"","type":"bytes"},{"name":"targets","type":"address[]"},{"name":"lengths","type":"uint256[]"},{"name":"values","type":"uint256[]"}],"name":"fire","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[],"name":"version","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[],"name":"owner","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"wallet","type":"address"}],"name":"authorize","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":true,"inputs":[{"name":"","type":"address"}],"name":"authorized","outputs":[{"name":"","type":"bool"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"","type":"address"},{"name":"","type":"uint256"},{"name":"","type":"bytes"}],"name":"tokenFallback","outputs":[],"payable":false,"stateMutability":"pure","type":"function"},{"constant":false,"inputs":[{"name":"wallet","type":"address"}],"name":"unauthorize","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"inputs":[{"name":"_owner","type":"address"}],"payable":false,"stateMutability":"nonpayable","type":"constructor"},{"payable":true,"stateMutability":"payable","type":"fallback"},{"anonymous":false,"inputs":[{"indexed":false,"name":"wallet","type":"address"}],"name":"Authorized","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"wallet","type":"address"}],"name":"Unauthorized","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"name":"wallet","type":"address"}],"name":"NewOwner","type":"event"}];

const getSetABI = [{"constant":true,"inputs":[],"name":"value","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":false,"inputs":[{"name":"value_","type":"uint256"}],"name":"setValue","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"inputs":[],"payable":false,"stateMutability":"nonpayable","type":"constructor"}];
const gunDeploymentABI = [{"constant":false,"inputs":[{"name":"_owner","type":"address"}],"name":"deployGatlingGun","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"inputs":[],"payable":false,"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":false,"name":"gunAddress","type":"address"},{"indexed":true,"name":"ownerAddress","type":"address"}],"name":"NewGunDeployed","type":"event"}];


const addresses = {
	"1": {
		"gunDeploymentAddress": "0x8784e79D4A357a9a790e209625902fC81B552323",
		"getSetAddresses": [
			"0xdEb40b38abB9eCF93b2c3A52496D654B5aBd0d99",
			"0x610C622d826C07BBc048E6002d5468d8BAF2C8F2",
			"0xFa8De71dCAB2B9EeCD4e9e1c474ED65e7A8992C2",
			"0xDb5a12F9De124d556bf51Fa387c03A16115Bfb21"
		]
	},
	"3": {
		"gunDeploymentAddress": "0x19A2c31a6E33EA2927B0C802c04d1aAb3de0c61f",
		"getSetAddresses": [
			"0x2031fAd254e7eB7DA8d13DD5fB01c1877b5F6554",
			"0xb9CCbc449a15d963E8B527234A7408a20b5878E1",
			"0xe2f67d2B9006422e6e2616a4e29ee234c1Ea3872",
			"0x4a33bB63CD3bFc6dC4961b132Fabb8b85315fF1b"
		]
	},
	"4": {
		"gunDeploymentAddress": "0xcf41f20e2338567153a25a8b107875871e416228",
		"getSetAddresses": [
			"0x064d7b9138ba818cCe30e3EA7B320291b077395c",
			"0x7F59999649Bb70d144db49D22E6D33997DFB00C3",
			"0xd18Fa1C77944F55Fe4DBF5B3d98CF68723776e19",
			"0x264c9613Cc83b1c750310BCeD085a463f1969e9B"
		]
	},
	"4927": {
		"gunDeploymentAddress": "0x00f60170eca711E1A0818023c960F2C795B11205",
		"getSetAddresses": [
			"0x342E58219fBC4EA5944a2Cd11705dc7BfEDA7F9F",
			"0x720D6aD0c3939f8f45573549f3730684a5737ccD",
			"0x67D328919A8cfc7Acf28D9AAFF376f3e61084Ef1",
			"0x9EA91B78488f4422353197E33A5ac14e63D4ec91"
		]
	}
}
