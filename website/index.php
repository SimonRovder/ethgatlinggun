<!DOCTYPE html>
<html lang="en">

<head>
  <?php include('head.html') ?>
  </head>
<body>
  <?php include('menu.html') ?>
  <!-- Page Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-left">
        <h1 class="mt-5">Ethereum Gatling Gun</h1>
        <p class="lead">Executing transactions on Ethereum 100x faster</p>
        <p>One of the main concerns around the current state of Ethereum (and blockchains in general) is the obvious lack of respectable responsiveness. The blockchains are slow, transactions can take minutes to complete, and even small peaks in activity of a single dApp can bring the entire system to it's knees.</p>
        <p>Enter, the <b>Ethereum Gatling Gun</b>. By hand-crafting sections of a Solidity smart contract directly in assembly, we can make a <b>smart contract act as a wallet</b>. Why is this useful? Well, a smart contract is code. And code allows you to iterate in loops. Given a payload that encodes multiple transactions, we can have the smart contract <b>fire a sequence of transactions in rapid succession</b>, all within a single transaction from the gun operator wallet.</p>
        <p>To get started head to the <a href="operation.php">Gatling Gun Operation</a> page. For an in-depth disection of how the whole thing works head to the <a href="https://medium.com/@simon.rovder/ethereum-gatling-gun-the-fastest-wallet-out-there-e05269f38da8">Medium article</a>.</p>
        <p>To see this wallet it action (and understand how to operate it), check out the demo video below:</p>
        <BR/>
        <p>
          <CENTER>
            <iframe width="560" height="315" src="https://www.youtube.com/embed/yY2QgMx_gh0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
          </CENTER>
        </p>
      </div>
    </div>
  </div>
  <?php include('footer.html') ?>
</body>

</html>
