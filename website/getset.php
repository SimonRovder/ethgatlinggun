<!DOCTYPE html>
<html lang="en">

<head>

  <?php include('head.html') ?>
  <SCRIPT language="JavaScript">

    function detectNetwork(err, netId) {
      initGetSetters(addresses[netId]['getSetAddresses']);
    }

    function setValue(address) {
      getSet = web3.eth.contract(getSetABI).at(address);
      getSet.setValue($("#" + address).val(), (err, res) => {
        if(err != undefined) {
          alert("Could not initiate the value setting process!");
        } else {
          alert("Value is being set... the blockchain is working.");
        }
      });
    }

    function initGetSetters(getSetAddresses) {
      for(i in getSetAddresses){
        address = getSetAddresses[i];
        document.getElementById('getsetholder').innerHTML += `
              <DIV>
                <STRONG>${address}</STRONG>
                <input class="form-control" type="text" id="v${address}" placeholder="" readonly>
                <div class="input-group">
                  <button class="btn btn-info" type="button" onclick="setValue('${address}')">Set value to: </button>
                  <input type="number" class="form-control" id="${address}" placeholder="Number here..">
                </div>
              </DIV>
              <HR/> `;

        (function(address){
          getSet = web3.eth.contract(getSetABI).at(address);
          getSet.value((err, res) => {
            $(`#v${address}`).val(`Value: ${res}`);
          });
        })(address);
      }
    }


    window.addEventListener('load', function () {
        if (typeof web3 !== 'undefined') {
            ethereum.enable();
            console.log('Web3 Detected! ' + web3.currentProvider.constructor.name);
            window.web3 = new Web3(web3.currentProvider);
            web3.version.getNetwork(detectNetwork);
        } else {
            alert("Metamask not installed! This site requires metamask!");
        }
    });


  </SCRIPT>

</head>

<body>
  <?php include('menu.html') ?>

  <div class="container">
    <div class="row">
      <div class="col-lg-12 text-left">
        <h1 class="mt-3">Get Set Contracts</h1>
      </div>
      <div class="col-lg-12 text-left" id="unknownNetwork" style="display: none">
        The Ethereum network you have set up in Metamask is not supported. Please use one of the following:
        <UL>
          <LI>Mainnet</LI>
          <LI>Ropsten</LI>
          <LI>Rinkeby</LI>
        </UL>
      </div>
      <div class="col-lg-12 text-left" id="getsetholder">
        <p>This page contains experimental GetSet contracts. You can view their current values and set them to a new value. You may need to refresh this page to see the up-to-date values.</p>
        <HR/>
      </div>
    </div>
  </div>
  <?php include('footer.html') ?>
</body>
</html>
